/**
 * Copyright (C) 2014 Brian Douglass bhdouglass@gmail.com
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

const Lang = imports.lang;
const St = imports.gi.St;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
const Main = imports.ui.main;
const Gio = imports.gi.Gio;
const Util = imports.misc.util;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

let indicator = null;

const RemindorIndicator = new Lang.Class({
	Name: 'RemindorIndicator',
	Extends: PanelMenu.Button,

	_init: function() {
		this.parent(St.Align.START);

		//Setup Icons
		this._activeIcon = Gio.icon_new_for_string(Me.path + '/img/remindor-active.svg');
		this._attentionIcon = Gio.icon_new_for_string(Me.path + '/img/remindor-attention.svg');

		this._appIcon = new St.Icon({
			gicon: this._activeIcon,
			icon_size: 20,
			style_class: 'system-status-icon'
		});

		//Setup button to show in the panel
		let button = new St.Button({
			child: this._appIcon
		});

		button.connect('clicked', Lang.bind(this, this._pressed));
		this.actor.add_actor(button);

		//Setup button menu items with callbacks
		this._add = new PopupMenu.PopupMenuItem('Add Reminder');
		this._add.connect('activate', Lang.bind(this, function() {
			Util.spawn(['indicator-remindor', '--add']);
		}));
		this.menu.addMenuItem(this._add);

		this._quick = new PopupMenu.PopupMenuItem('Quick Reminder');
		this._quick.connect('activate', Lang.bind(this, function() {
			Util.spawn(['indicator-remindor', '--quick']);
		}));
		this.menu.addMenuItem(this._quick);

		this._stop = new PopupMenu.PopupMenuItem('Stop Sound');
		this._stop.connect('activate', Lang.bind(this, function() {
			Util.spawn(['indicator-remindor', '--stop']);
		}));
		this.menu.addMenuItem(this._stop);

		this._manage = new PopupMenu.PopupMenuItem('Manage Reminders');
		this._manage.connect('activate', Lang.bind(this, function() {
			Util.spawn(['indicator-remindor', '--manage']);
		}));
		this.menu.addMenuItem(this._manage);

		this._prefs = new PopupMenu.PopupMenuItem('Preferences');
		this._prefs.connect('activate', Lang.bind(this, function() {
			Util.spawn(['indicator-remindor', '--preferences']);
		}));
		this.menu.addMenuItem(this._prefs);

		//Listen for Indicator Remindor over dbus
		this._connection = Gio.bus_get_sync(Gio.BusType.SESSION, null);
		this._signal = this._connection.signal_subscribe(
			null,
			'com.bhdouglass.indicator_remindor',
			null,
			'/com/bhdouglass/indicator_remindor/object',
			null,
			Gio.DBusSignalFlags.NONE,
			Lang.bind(this, this._signalCallback),
			null,
			null
		);
	},

	_pressed: function() {
		//Clear attention icon
		this._appIcon.gicon = this._activeIcon;

		if (this.menu) {
			this.menu.toggle();
		}
	},

	//Override
	_onButtonPress: function(actor, event) {
		this._pressed();
	},

	_signalCallback: function(connection, sender, object, int, signal, parameters, data) {
		let param = parameters.print(true);
		if (param.indexOf('attention') != -1) {
			this._appIcon.gicon = this._attentionIcon
		}
		else if (param.indexOf('active') != -1) {
			this._appIcon.gicon = this._activeIcon
		}
	}
});

function init() {}

function enable() {
	indicator = new RemindorIndicator();
	Main.panel.addToStatusArea('remindor', indicator);
}

function disable() {
	indicator.destroy();
	indicator = null;
}
