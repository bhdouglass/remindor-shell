Remindor-Shell
==============

Gnome Shell Extension for Indicator Remindor

**Note**: Requires [indicator-remindor](http://bhdouglass.com/remindor/) to be installed and running.

Version 2.0 has been tested against Gnome Shell 3.10 and 3.12.

For the old version that works with Gnome Shell 3.4.1 - 3.8 check out the
[pre-3.10 branch](https://github.com/bhdouglass/remindor-shell/tree/pre-3.10)

Installing
==========

~~~
mkdir -p ~/.local/share/gnome-shell/extensions/remindor-shell@bhdouglass.com/
git clone https://github.com/bhdouglass/remindor-shell.git ~/.local/share/gnome-shell/extensions/remindor-shell@bhdouglass.com/
~~~

**Or**

~~~
wget https://github.com/bhdouglass/remindor-shell/archive/master.zip
unzip master.zip
mkdir -p ~/.local/share/gnome-shell/extensions/
mv ~/.local/share/gnome-shell/extensions/remindor-shell@bhdouglass.com/
~~~

License
=======

Copyright (C) 2014 Brian Douglass

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published 
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.